import path from 'path';
import { Configuration } from 'webpack';
import { optionTypes } from './types/webpack/types/webpackTypes';
import { webpackConfig } from './types/webpack/webpackConfig';

type BuildMode = 'development' | 'production';
export interface BuildEnv {
  mode: BuildMode;
  port: number;
}

export default (env: BuildEnv) => {
  const paths = {
    pathToHtml: path.join(__dirname, 'public', 'index.html'),
    pathToSrc: path.join(__dirname, 'src'),
    pathToEntry: path.join(__dirname, 'src', 'index.tsx'),
    pathOfBuild: path.resolve(__dirname, 'dist'),
  };

  const mode = env.mode || 'development';
  const PORT = env.port || 3010;

  const options: optionTypes = {
    mode,
    paths,
    isDev: true,
    port: PORT,
  };

  const config: Configuration = webpackConfig(options);

  return config;
};
