type ticTac = 'x' | 'o' | null;

export function checkMove(field: Array<ticTac[]>) {
  const diagonal = [];
  const diagonalReverse = [field[0][2], field[1][1], field[2][0]];
  const draw = [];
  let table = [];
  let j = 0;
  for (let array = 0; array < field.length; array += 1) {
    if (field[array].every((rowValue) => rowValue === 'x')) return 'крестики выиграли';
    if (field[array].every((rowValue) => rowValue === 'o')) return 'нолики выиграли';

    for (let val = 0; val < field[array].length; val += 1) {
      table.push(field[val][j]);

      if (val === array) {
        diagonal.push(field[array][val]);
      }
      draw.push(field[array][val]);
    }
    if (table.every((el) => el === 'x')) return 'крестики выиграли';
    if (table.every((el) => el === 'o')) return 'нолики выиграли';
    table = [];
    j += 1;
  }
  if (diagonal.every((el) => el === 'x')) return 'крестики выиграли';
  if (diagonal.every((el) => el === 'o')) return 'нолики выиграли';

  if (diagonalReverse.every((el) => el === 'x')) return 'крестики выиграли';
  if (diagonalReverse.every((el) => el === 'o')) return 'нолики выиграли';
  if (draw.every((el) => el !== null)) return 'DRAW';
  return 'next move';
}
