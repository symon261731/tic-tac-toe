import { TicTacSide } from 'shared/UIKIT/Message/Message';
import { genderType } from 'shared/UIKIT/PlayerRow/PlayerRow';
import { statusState } from 'shared/UIKIT/Status/Status';

export const fakeMessages = [
  { name: 'Плюшкина Екатерина', side: TicTacSide.CROSS, text: 'Ну что, готовься к поражению!!1' },
  { name: 'Пупкин Владлен', side: TicTacSide.CIRCLE, text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет' },
  { name: 'Пупкин Владлен', side: TicTacSide.CIRCLE, text: 'Я туплю…' },
  { name: 'Плюшкина Екатерина', side: TicTacSide.CROSS, text: 'Я маленький программер, Пишу свой говнокод Он тормозит и глючит, Но пашет как-то вот...' },
];

export const playerListPageFakeData = [
  {
    name: 'Александров Игнат Анатолиевич', age: 30, gender: genderType.MALE, status: statusState.BLOKED, created: '12 октября 2021', changed: '12 октября 2021',
  },
  {
    name: 'Мартынов Остап Фёдорович', age: 12, gender: genderType.MALE, status: statusState.READY, created: '12 октября 2021', changed: '12 октября 2021',
  },
  {
    name: 'Комаров Цефас Александрович', age: 83, gender: genderType.MALE, status: statusState.BLOKED, created: '12 октября 2021', changed: '12 октября 2021',
  },
  {
    name: 'Кулаков Станислав Петрович', age: 43, gender: genderType.FEMALE, status: statusState.BLOKED, created: '12 октября 2021', changed: '12 октября 2021',
  },
];

export const activePlayerPageFakeData = [
  { name: 'Александров Игнат Анатолиевич', status: statusState.ALREADY_PLAY },
  { name: 'Василенко Эрик Платонович', status: statusState.READY },
  { name: 'Быков Юрий Виталиевич', status: statusState.ALREADY_PLAY },
  { name: 'Галкин Феликс Платонович', status: statusState.READY },
];

export const RatingWindowWidgetFakeData = [
  {
    name: 'Александров Игнат Анатолиевич', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Шевченко Рафаил Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Мазайло Трофим Артёмович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Логинов Остин Данилович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Борисов Йошка Васильевич', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Ждсан Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Ждамн Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Ждаафн Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Ждн Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Ждац Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Жданч Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
  {
    name: 'Соловьёв Жданф Михайлович', allGames: 24534, win: 9824, lose: 1222, persent: '87%',
  },
];

export const HistoryListFakeData = [
  {
    firstPlayer: 'Терещенко У. Р.',
    secondPlayer: 'Многогрешный П. В.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Терещенко У. Р.',
  },
  {
    firstPlayer: 'Горбачёв А. Д.',
    secondPlayer: 'Многогрешный П. В.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Многогрешный П. В.',
  },
  {
    firstPlayer: 'Константинов В. Н.',
    secondPlayer: 'Сасько Ц. А.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Константинов В. Н.',
  },
  {
    firstPlayer: 'Никифорова Б. А.',
    secondPlayer: 'Горбачёв А. Д.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Никифорова Б. А.',
  },
  {
    firstPlayer: 'Кулишенко К. И.',
    secondPlayer: 'Вишняков Ч. М.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Вишняков Ч. М.',
  },
  {
    firstPlayer: 'Гриневска Д. Б.',
    secondPlayer: 'Кудрявцев Э. В.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Гриневска Д. Б.',
  },
  {
    firstPlayer: 'Нестеров Х. А.',
    secondPlayer: 'Исаева О. А.',
    date: '12 февраля 2022',
    time: '43 мин 13 сек',
    winner: 'Исаева О. А.',
  },

];
