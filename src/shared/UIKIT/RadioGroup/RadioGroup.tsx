/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';
import { classNames } from 'shared/helpers';
import ManSvg from 'shared/public/svg/Emoji/man.svg';
import WomenSvg from 'shared/public/svg/Emoji/women.svg';
import styles from './RadioGroup.module.scss';

interface RadioGroupProps {
    className?: string,

}

export enum genderValue {
    MALE = 'male',
    FEMALE = 'female'
}

export const RadioGroup = (props: RadioGroupProps) => {
  const { className } = props;
  const [checked, setChecked] = useState(0);

  return (
    <div className={classNames(styles.radioGroup, {}, [className])}>
      <input
        className={styles.radioInput}
        id="female"
        type="radio"
        value={genderValue.FEMALE}
        name="gender"
        onChange={() => setChecked(1)}
      />
      <label
        htmlFor="female"
        className={
          classNames(styles.radioContainer, { [styles.checked]: checked === 1 }, [])
        }
      >
        <WomenSvg className={styles.radio} />
      </label>
      <input
        id="male"
        type="radio"
        value={genderValue.MALE}
        name="gender"
        className={styles.radioInput}
        onChange={() => setChecked(2)}
      />
      <label
        htmlFor="male"
        className={classNames(styles.radioContainer, { [styles.checked]: checked === 2 }, [])}
      >
        <ManSvg className={styles.radio} />
      </label>
    </div>
  );
};
