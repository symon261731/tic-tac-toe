import React, { FC, ReactNode } from 'react';
import { Link, LinkProps } from 'react-router-dom';
import { classNames } from 'shared/helpers';
import styles from './NavLink.module.scss';

interface NavLinkProps extends LinkProps {
    className?: string,
    to: string,
    children: ReactNode,
    active?: boolean
}

export const NavLink: FC<NavLinkProps> = (props) => {
  const {
    className, to, children, active = false, ...otherProps
  } = props;
  return (
    <Link
      to={to}
      className={classNames(styles.Link, { [styles.active]: active }, [className!])}
      {...otherProps}
    >
      {children}
    </Link>
  );
};
