import React from 'react';
import { classNames } from 'shared/helpers';

import styles from './Status.module.scss';

export enum statusState{
    READY = 'ready',
    BLOKED = 'blocked',
    ALREADY_PLAY = 'already_play',
    LOGOUT = 'logout',
}

interface StatusProps {
    className?: string,
    statusValue?: statusState
}

export const Status = (props: StatusProps) => {
  const { className, statusValue = statusState.LOGOUT } = props;

  let content = '';
  switch (statusValue) {
  case statusState.ALREADY_PLAY: content = 'Играет';
    break;
  case statusState.BLOKED: content = 'Заблокирован';
    break;
  case statusState.LOGOUT: content = 'Вне игры';
    break;
  case statusState.READY: content = 'Готов';
    break;
  default: content = 'Неизвестный статус';
  }

  return (
    <div className={classNames(styles.Status, {}, [className, styles[statusValue]])}>
      {content}
    </div>
  );
};
