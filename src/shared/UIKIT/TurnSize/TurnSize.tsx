import { gameFieldMob } from 'entities/GameField';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import { classNames } from 'shared/helpers';
import Circle from 'shared/public/svg/smallCircle.svg';
import Cross from 'shared/public/svg/tic.svg';
import styles from './TurnSize.module.scss';

interface TurnSizeProps {
    className?: string,

}

export const TurnSize: FC<TurnSizeProps> = observer((props) => {
  const { className } = props;
  const { currentPlayer } = gameFieldMob;

  return (
    <div className={classNames(styles.TurnSize, {}, [className])}>
      <p>Ходит</p>
      {currentPlayer.size === 'o' ? <Circle /> : <Cross />}
      <p>{currentPlayer.username}</p>
    </div>
  );
});
