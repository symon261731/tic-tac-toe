import React from 'react';
import { classNames } from 'shared/helpers';
import styles from './Spinner.module.scss';

export const Spinner = () => (
  <div className={classNames(styles.spinner, {}, [])}>
    <svg width="150" height="150" stroke="#60C2AA" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">

      <g className={styles.spinner_V8m1}><circle cx="12" cy="12" r="9.5" fill="none" strokeWidth="3" /></g>

    </svg>

  </div>
);
