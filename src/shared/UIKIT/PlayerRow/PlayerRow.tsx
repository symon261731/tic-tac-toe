import React, { FC } from 'react';
import { classNames } from 'shared/helpers';
import MaleEmoji from 'shared/public/svg/Emoji/man.svg';
import FemaleEmoji from 'shared/public/svg/Emoji/women.svg';
import { Button, buttonColor } from '../Button/Button';
import { Status, statusState } from '../Status/Status';
import styles from './PlayerRow.module.scss';

export enum genderType {
  MALE = 'male',
  FEMALE = 'female'
}
interface PlayerRowProps {
    className?: string,
    name?: string,
    age?: string | number,
    gender?: genderType,
    status?: statusState.BLOKED | statusState.READY,
    created?: string,
    changed?: string,

}

export const PlayerRow: FC<PlayerRowProps> = (props) => {
  const {
    className, name, age, gender, status, created, changed,
  } = props;

  return (
    <div className={classNames(styles.row, {}, [className])}>
      <p className={classNames(styles.name, {}, [])}>{name}</p>
      <p className={classNames(styles.age)}>{`${age}`}</p>
      <div className={classNames(styles.emoji, {}, [])}>
        {gender === genderType.FEMALE ? <FemaleEmoji /> : <MaleEmoji />}
      </div>
      <Status statusValue={status} className={styles.status} />
      <div className={styles.date}>{created}</div>
      <div className={styles.date}>{changed}</div>
      <Button
        className={styles.button}
        banWithIcon={status === statusState.BLOKED}
        color={buttonColor.GREY}
      >
        {status === statusState.BLOKED ? 'Разблокировать' : 'Заблокировать'}
      </Button>
    </div>
  );
};
