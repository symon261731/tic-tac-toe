/* eslint-disable no-nested-ternary */
import { ButtonHTMLAttributes, FC } from 'react';
import { classNames } from 'shared/helpers';
import CrossSvg from 'shared/public/TicTac/Vector.svg';
import CircleSvg from 'shared/public/TicTac/zero.svg';
import styles from './Square.module.scss';

interface SquareProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string,
    value: string | null,
}

export const Square: FC<SquareProps> = (props) => {
  const { value, className, ...otherProps } = props;

  return (
    <button
      type="button"
      className={classNames(styles.cell, {}, [className])}
      {...otherProps}
    >
      {value === 'x' ? <CrossSvg /> : value === 'o' ? <CircleSvg /> : null}
    </button>
  );
};
