import React from 'react';
import { classNames } from 'shared/helpers';
import LogoImage from 'shared/public/logo/logo.svg';
import styles from './Logo.module.scss';

export const Logo = () => (
  <div className={classNames(styles.Logo)}>
    <LogoImage />
  </div>
);
