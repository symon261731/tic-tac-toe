import React, { ReactNode, ButtonHTMLAttributes, FC } from 'react';
import { classNames } from 'shared/helpers';
import BanSvg from 'shared/public/svg/ban.svg';
import Arrow from 'shared/public/svg/SendMessageSvg.svg';
import styles from './Button.module.scss';

export enum buttonTheme{
    NORMAL = 'normal',
    CLEAR = 'clear',
    MESSAGE = 'message'
}

export enum buttonSize {
  S = 'size_s',
  M = 'size_m'
}

export enum buttonColor{
  GREEN = 'green',
  GREY = 'grey'
}
interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string,
    children?: ReactNode,
    theme?: buttonTheme,
    disabled?: boolean,
    banWithIcon?: boolean,
    size?: buttonSize
}

export const Button: FC<ButtonProps> = (props) => {
  const {
    className,
    color = buttonColor.GREEN,
    disabled = false,
    theme = buttonTheme.NORMAL,
    banWithIcon = false,
    children,
    size = buttonSize.M,
    ...otherProps
  } = props;

  const mods = {
    [styles.disabled]: disabled,
    [styles.ban]: banWithIcon,
  };
  const stylesArray = [className, styles[color], styles[theme], styles[size]];
  return (
    <button
      className={classNames(styles.button, mods, stylesArray)}
      type="button"
      disabled={disabled}
      {...otherProps}
    >
      { theme === buttonTheme.MESSAGE ? <Arrow /> : null}
      { banWithIcon ? <BanSvg /> : null}
      {children}
    </button>
  );
};
