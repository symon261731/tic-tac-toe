import React, { FC, InputHTMLAttributes } from 'react';
import { classNames } from 'shared/helpers';
import styles from './Input.module.scss';

export enum InputTheme {
    NORMAL = 'normal'
}

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    className?: string,
    placeholder?: string,
    theme?: InputTheme,
    error?: boolean,
    type?: string
}

export const Input : FC<InputProps> = (props) => {
  const {
    className, type = 'text', error, placeholder = '', theme = InputTheme.NORMAL, ...otherprops
  } = props;

  return (
    <input
      placeholder={placeholder}
      type={type}
      className={classNames(styles.Input, { [styles.error]: error }, [className, styles[theme]])}
      {...otherprops}
    />
  );
};
