import React, { FC } from 'react';
import { classNames } from 'shared/helpers';
import { Button } from 'shared/UIKIT/Button/Button';
import { Status, statusState } from 'shared/UIKIT/Status/Status';
import styles from './ActivePlayerRow.module.scss';

interface ActivePlayerRowProps {
    className?: string,
    status?: statusState,
    name?: string,
}

export const ActivePlayerRow: FC<ActivePlayerRowProps> = (props) => {
  const { className, name = '', status } = props;
  return (
    <div className={classNames(styles.activePlayerRow, {}, [className])}>
      <p className={styles.name}>{name}</p>
      <Status statusValue={status} className={styles.statusWidth} />
      <Button
        disabled={status === statusState.ALREADY_PLAY}
      >
        Позвать играть
      </Button>
    </div>
  );
};
