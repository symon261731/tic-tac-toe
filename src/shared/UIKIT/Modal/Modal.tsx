/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, {
  FC, ReactNode, useCallback, useEffect,
} from 'react';
import { classNames } from 'shared/helpers';
import CloseIcon from 'shared/public/svg/closeIcon.svg';
import { Button, buttonTheme } from '../Button/Button';
import styles from './Modal.module.scss';

export interface ModalProps {
    className?: string,
    isOpen: boolean,
    onClose: ()=> void,
    children: ReactNode,
}

export const Modal : FC<ModalProps> = (props) => {
  const {
    className, isOpen, onClose, children,
  } = props;

  const closeWindow = useCallback(() => {
    onClose();
  }, [onClose]);

  const closeWindowWithKeyEsc = (e: KeyboardEvent) => {
    if (e.key === 'Escape') {
      closeWindow();
    }
  };

  const onClickContent = (e: React.MouseEvent) => {
    e.stopPropagation();
  };
  useEffect(() => {
    document.addEventListener('keydown', closeWindowWithKeyEsc);

    return () => document.removeEventListener('keydown', closeWindowWithKeyEsc);
  }, [isOpen]);

  return (
    <div className={classNames(styles.Modal, { [styles.show]: isOpen }, [className])}>
      <div onClick={closeWindow} className={classNames(styles.background)}>
        <div
          onClick={onClickContent}
          className={classNames(styles.modalWindow, { [styles.showContent]: isOpen }, [])}
        >
          <Button onClick={closeWindow} theme={buttonTheme.CLEAR} className={styles.button}>
            <CloseIcon />
          </Button>
          {children}
        </div>
      </div>

    </div>
  );
};
