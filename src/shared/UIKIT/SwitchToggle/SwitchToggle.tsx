/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { classNames } from 'shared/helpers';
import styles from './SwitchToggle.module.scss';

interface SwitchToggleProps {
    className?: string,
    checked? : boolean,
    changeChecked? : ()=> void,
}

export const SwitchToggle = (props: SwitchToggleProps) => {
  const { className, checked, changeChecked } = props;
  return (
    <label className={classNames(styles.switch, {}, [className])}>
      <input onChange={changeChecked} checked={checked} type="checkbox" className={styles.toggleInput} />
      <span className={classNames(styles.slider, {}, [styles.round])} />
    </label>
  );
};
