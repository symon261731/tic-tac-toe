import React, { FC } from 'react';
import { classNames } from 'shared/helpers';
import styles from './Message.module.scss';

export enum TicTacSide{
    CROSS = 'cross',
    CIRCLE = 'circle'
}

interface MessageProps{
    className?: string,
    side?: TicTacSide
    name?: string,
    text?: string
}

export const Message: FC<MessageProps> = (props) => {
  const {
    className, side, name, text,
  } = props;

  return (
    <div className={classNames(styles.message, {}, [className, styles[side]])}>
      <div className={styles.header}>
        <p className={classNames(styles.name, {}, [])}>{name}</p>
        <p className={styles.time}>13:45</p>
      </div>
      <p className={styles.text}>{text}</p>
    </div>
  );
};
