import React, { FC, ReactNode } from 'react';
import { classNames } from 'shared/helpers';
import styles from './Window.module.scss';

interface WindowProps {
    className?: string,
    width?: number,
    center?: boolean,
    children: ReactNode,
}

export const Window: FC<WindowProps> = (props) => {
  const {
    className, width = 1600, center = false, children,
  } = props;

  return (
    <div
      style={{ maxWidth: `${width}px` }}
      className={classNames(styles.Window, { [styles.center]: center }, [className])}
    >
      {children}
    </div>
  );
};
