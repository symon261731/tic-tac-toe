import { makeAutoObservable } from 'mobx';

interface userProps{
    username: string,
    password: string,
}

class UserStore {
  public user = {
    username: 'admin',
  };

  constructor() {
    makeAutoObservable(this);
  }

  logout() {
    this.user.username = '';
  }

  async login({ username, password }: userProps) {
    // псевдокод
    // const request = await axios('https://localhost:3002/user/login', { username, password });
    // this.user.username = request.data.username;

    this.user.username = username;
  }
}

export default UserStore;
