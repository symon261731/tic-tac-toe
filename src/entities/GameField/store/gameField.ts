import { makeAutoObservable, runInAction } from 'mobx';

export interface playerProps{
    username: string,
    size: 'x' | 'o',
  }

const firstPlayer:playerProps = { username: 'vasya pupkin', size: 'x' };
const secondPlayer: playerProps = { username: 'katya supkina', size: 'o' };

class GameField {
  field: any[][] = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ];

  currentPlayer = firstPlayer;

  isGameOver = false;

  winnerName = '';

  constructor() {
    makeAutoObservable(this);
  }

  setOneTurn(newField: any[][]) {
    runInAction(() => {
      this.field = newField;
    });
  }

  setWinnerPlayer(value:string) {
    switch (value) {
    case 'крестики выиграли': this.winnerName = firstPlayer.username;
      break;
    case 'нолики выиграли': this.winnerName = secondPlayer.username;
      break;
    case 'DRAW': this.winnerName = 'никто из вас не ';
      break;
    default: throw new Error('что то пошло не по плану');
    }
  }

  finishGame() {
    this.isGameOver = true;
  }

  changePlayer() {
    if (this.currentPlayer.size === 'x') {
      this.currentPlayer = secondPlayer;
    } else {
      this.currentPlayer = firstPlayer;
    }
  }
}

export default GameField;
