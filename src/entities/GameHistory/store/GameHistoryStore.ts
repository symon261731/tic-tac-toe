import { makeAutoObservable } from 'mobx';

class GameHistoryStore {
  pending: boolean = false;

  error: string = '';

  constructor() {
    makeAutoObservable(this);
  }

  // eslint-disable-next-line consistent-return
  async fetchHistory() {
    try {
      this.pending = true;
      const result = await fetch('http://localhost:3020/history')
        .then((res) => res.json());
      this.pending = false;
      return result;
    } catch (e) {
      this.pending = false;
      this.error = 'Произошла ошибка пожалуйста зайдите позже';
      // eslint-disable-next-line no-console
      console.log(e);
    }
  }
}

const GameHistoryMob = new GameHistoryStore();

export default GameHistoryMob;
