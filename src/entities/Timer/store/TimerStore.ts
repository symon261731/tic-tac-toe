import { makeAutoObservable } from 'mobx';

class TimerMob {
  secondsPassed = 0;

  minutesPassed = 0;

  resultMinutes = 0;

  resultSecond = 0;

  constructor() {
    makeAutoObservable(this);
  }

  increaseSeconds() {
    this.secondsPassed += 1;
  }

  increaseMinites() {
    this.minutesPassed += 1;
  }

  stop() {
    this.resultMinutes = this.minutesPassed;
    this.resultSecond = this.secondsPassed;
  }

  reset() {
    this.secondsPassed = 0;
    this.minutesPassed = 0;
  }
}

export default TimerMob;
