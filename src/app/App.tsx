import { useEffect } from 'react';
import { userMob } from 'entities/user';
import { observer } from 'mobx-react';
import { useNavigate } from 'react-router-dom';
import { AuthorizationPageAsync } from 'pages';
import { Navbar } from 'widgets/Navbar/Navbar';
import AppRouter from './Routes/AppRouter';
import './styles/index.scss';

const App = observer(() => {
  const navigate = useNavigate();

  useEffect(() => {
    if (userMob.user.username === '') {
      navigate('/auth');
    }
  }, [userMob.user.username]);

  if (window.location.pathname === '/auth') {
    return <AuthorizationPageAsync />;
  }

  return (
    <div className="app">
      <Navbar />
      <AppRouter />
    </div>
  );
});

export default App;
