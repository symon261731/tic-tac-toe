import React, { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import {
  ActivePlayersPageAsync,
  AuthorizationPageAsync,
  GameFieldPageAsync,
  HistoryPageAsync,
  NotFoundPageAsync,
  PlayerListPageAsync,
  RatingPageAsync,
} from 'pages';
import { Spinner } from 'shared/UIKIT/Spinner/Spinner';

const AppRouter = () => (
  <Suspense fallback={<Spinner />}>
    <Routes>
      <Route path="/auth" element={<AuthorizationPageAsync />} />
      <Route path="/gameField" element={<GameFieldPageAsync />} />
      <Route path="/activePlayers" element={<ActivePlayersPageAsync />} />
      <Route path="/history" element={<HistoryPageAsync />} />
      <Route path="/rating" element={<RatingPageAsync />} />
      <Route path="/playerList" element={<PlayerListPageAsync />} />
      <Route path="/*" element={<NotFoundPageAsync />} />
    </Routes>
  </Suspense>
);

export default AppRouter;
