import React, {
  ChangeEvent, useCallback, useState,
} from 'react';
import { classNames } from 'shared/helpers';
import { Button, buttonTheme } from 'shared/UIKIT/Button/Button';
import { Input } from 'shared/UIKIT/Input/Input';
import { RadioGroup } from 'shared/UIKIT/RadioGroup/RadioGroup';

import styles from './AddPlayerForm.module.scss';

interface AddPlayerFormProps {
    className?: string,
}

export const AddPlayerForm = (props: AddPlayerFormProps) => {
  const { className } = props;

  const [number, setNumber] = useState('');

  const validateNumberInput = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    if (Number(e.target.value) > 99) {
      setNumber((prev) => prev);
    } else {
      setNumber(e.target.value);
    }
  }, [number]);

  return (
    <div className={classNames(styles.addPlayerForm, {}, [className])}>
      <h2 className={classNames(styles.title)}>Добавьте игрока</h2>
      <div className={classNames(styles.InputLabel, {}, [styles.widthInputName])}>
        <p className={styles.label}>ФИО</p>
        <Input placeholder="Иванов Иван Иванович" id="login" />
      </div>

      <div className={styles.inputGroup}>
        <div className={classNames(styles.InputLabel, {}, [styles.inputGroupAgeInput])}>
          <p className={styles.label}>Возраст</p>
          <Input value={number} onChange={(e: ChangeEvent<HTMLInputElement>) => validateNumberInput(e)} type="number" placeholder="0" className={styles.InputNumber} />
        </div>
        <div className={styles.InputLabel}>
          <p className={styles.label}>Пол</p>
          <RadioGroup />
        </div>
      </div>
      <Button theme={buttonTheme.NORMAL} className={classNames(styles.button)}>Добавить</Button>
    </div>
  );
};
