import React, { FC } from 'react';
import { Modal } from 'shared/UIKIT/Modal/Modal';
import { AddPlayerForm } from '../AddPlayerForm/AddPlayerForm';

interface AddPlayerModalProps {
    isOpen: boolean,
    onClose: ()=>void
}

export const AddPlayerModal: FC<AddPlayerModalProps> = (props) => {
  const { isOpen, onClose } = props;

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <AddPlayerForm />
    </Modal>
  );
};
