import React, {
  FC, useCallback, useEffect, useState,
} from 'react';
import { classNames } from 'shared/helpers';
import { Button, buttonTheme } from 'shared/UIKIT/Button/Button';
import { Input } from 'shared/UIKIT/Input/Input';
import { TicTacSide } from 'shared/UIKIT/Message/Message';
import styles from './SendMessage.module.scss';

interface SendMessageFormProps {
    className?: string,
    sendMessages : (value: {name:string, side:TicTacSide, text:string})=> void,
}

export const SendMessageForm: FC<SendMessageFormProps> = (props) => {
  const { className, sendMessages } = props;

  const [messageValue, setMessageValue] = useState('');
  const sendMessage = useCallback(() => {
    if (messageValue) {
      sendMessages({
        name: 'тестовый юзер',
        side: TicTacSide.CROSS,
        text: messageValue,
      });
      setMessageValue('');
    }
  }, [messageValue]);

  const sendMessageWithKey = (e: KeyboardEvent) => {
    if (e.key === 'Enter') {
      sendMessage();
    }
  };

  const writeMessage = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setMessageValue(e.target.value);
  }, [messageValue]);

  useEffect(() => {
    document.addEventListener('keydown', sendMessageWithKey);

    return () => document.removeEventListener('keydown', sendMessageWithKey);
  }, [sendMessage]);

  return (
    <div className={classNames(styles.sendMessageInputForm, {}, [className])}>
      <Input value={messageValue} onChange={(e) => writeMessage(e)} placeholder="Сообщение..." className={styles.sendMessageInput} />
      <Button
        onClick={sendMessage}
        theme={buttonTheme.MESSAGE}
        className={styles.button}
      />
    </div>
  );
};
