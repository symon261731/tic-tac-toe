/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useCallback, useState } from 'react';
import { classNames } from 'shared/helpers';
import { Button } from 'shared/UIKIT/Button/Button';
import { Input } from 'shared/UIKIT/Input/Input';
import styles from './AuthByLoginForm.module.scss';

interface AuthByLoginFormProps {
    className?: string,
}

type InputChangeEvent = React.ChangeEvent<HTMLInputElement>;

export const AuthByLoginForm = (props: AuthByLoginFormProps) => {
  const { className } = props;
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [isLoginInputVisited, setIsLoginInputVisited] = useState(false);
  const [isPasswordInputVisited, setIsPasswordInputVisited] = useState(false);
  const [loginError, setLoginError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const onBlurChangeCheck = (e: InputChangeEvent) => {
    switch (e.target.name) {
    case ('login'): setIsLoginInputVisited(true);
      break;
    case ('password'): setIsPasswordInputVisited(true);
      break;
    default:
    }
  };

  const loginChangeHandler = useCallback((e: InputChangeEvent) => {
    // if(e.target.value.includes('*' || ' ')) setLoginError(true);
    const regLogin = /^(?=.*[A-Za-z0-9]$)[A-Za-z][A-Za-z\d]{0,19}$/;
    if (!regLogin.test(e.target.value)) {
      setLoginError(true);
    } else {
      setLoginError(false);
    }
    setLogin(e.target.value);
  }, [login]);

  const passwordChangeHandler = (e: InputChangeEvent) => {
    //! минимальная длинна  длина 8 символов
    //! if (e.targer.value.length < 8 ) setPasswordError(true) Но я решил делать регуляркой)

    const regPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (!regPassword.test(e.target.value)) {
      setPasswordError(true);
    } else {
      setPasswordError(false);
    }
    setPassword(e.target.value);
  };

  const pasteDefence = (e: React.ClipboardEvent<HTMLInputElement>) => {
    e.preventDefault();
  };

  return (
    <div className={classNames(styles.formContainer, {}, [className])}>
      <div className={classNames(styles.inputPare)}>
        <Input
          name="login"
          value={login}
          onChange={(e) => loginChangeHandler(e)}
          onPaste={(e) => pasteDefence(e)}
          onFocus={() => setIsLoginInputVisited(false)}
          onBlur={(e) => onBlurChangeCheck(e)}
          error={loginError}
          className={styles.authFormInput}
          placeholder="Логин"
        />
        {isLoginInputVisited && loginError
          ? <span className={styles.errorMessage}>Неверный логин</span>
          : null}
      </div>
      <div>
        <Input
          type="password"
          name="password"
          value={password}
          onChange={(e) => passwordChangeHandler(e)}
          onPaste={(e) => pasteDefence(e)}
          onFocus={() => setIsPasswordInputVisited(false)}
          onBlur={(e) => onBlurChangeCheck(e)}
          error={passwordError}
          className={styles.authFormInput}
          placeholder="Пароль"
        />
        {isPasswordInputVisited && passwordError
          ? <span className={styles.errorMessage}>Неверный Пароль</span>
          : null}
      </div>
      <Button>Войти</Button>
    </div>
  );
};
