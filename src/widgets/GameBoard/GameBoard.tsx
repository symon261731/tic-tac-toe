import { observer } from 'mobx-react';
import { FC } from 'react';
import { classNames, checkMove } from 'shared/helpers';
import { Square } from 'shared/UIKIT/Square/Square';
import { gameFieldMob } from 'entities/GameField';
import styles from './GameBoard.module.scss';

interface GameBoardProps {
    className?: string,
}

export const GameBoard: FC<GameBoardProps> = observer((props: GameBoardProps) => {
  const { className } = props;
  let i = 0;
  const { field, currentPlayer } = gameFieldMob;

  const setOneMove = (val: null | string, rowIndex:number, squareIndex:number) => {
    if (val === null) {
      const newValue = field;
      newValue[rowIndex][squareIndex] = currentPlayer.size;
      gameFieldMob.setOneTurn(newValue);
      console.log(field);
      if (checkMove(field) === 'next move') {
        gameFieldMob.changePlayer();
        return;
      }
      gameFieldMob.setWinnerPlayer(checkMove(field));
      gameFieldMob.finishGame();
    }
  };

  return (
    <div className={classNames(styles.GameBoard, {}, [className])}>
      {/* {field.map((row, rowIndex) => row.map((square, squareIndex) => {
        i += 1;
        return (
          <Square
            key={i}
            onClick={() => {
              setOneMove(square, rowIndex, squareIndex);
            }}
            value={square}
          />
        );
      }))} */}
      {field.map((row, rowIndex) => (
        // eslint-disable-next-line react/no-array-index-key
        <div className={styles.row} key={rowIndex}>
          {row.map((square, squareIndex) => {
            i += 1;
            return (
              <Square
                key={i}
                onClick={() => {
                  setOneMove(square, rowIndex, squareIndex);
                }}
                value={square}
              />
            );
          })}
        </div>
      ))}
    </div>
  );
});
