import { SendMessageForm } from 'features/SendMessageForm';
import { FC, useEffect, useState } from 'react';
import { classNames } from 'shared/helpers';
import { Message, TicTacSide } from 'shared/UIKIT/Message/Message';
import { fakeMessages } from 'shared/mockData/mockData';
import styles from './Chat.module.scss';

interface ChatProps{
    className?: string,
    messagesArray?: any[],
}

interface MessageProp{
  name: string, side: TicTacSide, text:string
}

export const Chat: FC<ChatProps> = (props) => {
  const { className, messagesArray } = props;

  const messages = fakeMessages;

  const [topSideFade, setTopSideFade] = useState(false);

  // ? для теста
  const [testMessages, setTestMessages] = useState(messages);

  useEffect(() => {
    const blockMessages = document.getElementById('messages');
    if (blockMessages.scrollHeight > blockMessages.clientHeight) {
      setTopSideFade(true);
      blockMessages.scrollTop = 10000;
    }
  }, [testMessages]);

  return (
    <div className={classNames(styles.Chat, {}, [className])}>
      <div className={classNames(styles.fadeTop, { [styles.fadeTopVisible]: topSideFade }, [])} />
      <div id="messages" className={styles.messages}>
        {testMessages ? testMessages.map((oneMessage) => (
          <Message
            key={oneMessage.text}
            name={oneMessage.name}
            text={oneMessage.text}
            side={oneMessage.side}
          />
        )) : <h2 className={styles.notYetMessages}>Сообщений еще нет</h2>}
      </div>
      <div className={styles.fade} />
      <SendMessageForm
        sendMessages={(value: MessageProp) => setTestMessages((prev) => ([...prev, value]))}
      />
    </div>
  );
};
