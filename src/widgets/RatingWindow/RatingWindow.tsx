import React from 'react';
import { Window } from 'shared/UIKIT/Window/Window';
import { RatingList } from 'widgets/RatingList/RatingList';
import { RatingWindowWidgetFakeData } from 'shared/mockData/mockData';
import styles from './RatingWindow.module.scss';

export const RatingWindow = () => {
  const fakeData = RatingWindowWidgetFakeData;
  return (
    <Window width={1026} center>
      <h2 className={styles.title}>Рейтинг игроков</h2>
      <RatingList list={fakeData} />
    </Window>
  );
};
