import { useEffect } from 'react';
import { classNames } from 'shared/helpers';
import { observer } from 'mobx-react';
import { testTimer } from 'entities/Timer';
import styles from './Timer.module.scss';

interface TimerProps {
  className?: string,
}

// const testTimer = new TimerMob();

export const Timer = observer((props: TimerProps) => {
  const { className } = props;

  useEffect(() => {
    const interval = setInterval(() => {
      testTimer.increaseSeconds();
    }, 1000);
    return () => {
      clearInterval(interval);
      testTimer.reset();
    };
  }, []);

  if (testTimer.secondsPassed === 60) {
    testTimer.secondsPassed = 0;
    testTimer.increaseMinites();
  }

  return (
    <div className={classNames(styles.timer, {}, [className])}>
      <div className={styles.content}>
        {testTimer.minutesPassed <= 9 ? `0${testTimer.minutesPassed}` : testTimer.minutesPassed}
      </div>
      <div className={styles.content}>:</div>
      <div className={styles.content}>
        {testTimer.secondsPassed <= 9 ? `0${testTimer.secondsPassed}` : testTimer.secondsPassed}
      </div>
    </div>
  );
});
