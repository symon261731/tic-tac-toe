import React from 'react';
import { classNames } from 'shared/helpers';
import { statusState } from 'shared/UIKIT/Status/Status';
import { ActivePlayerRow } from 'shared/UIKIT/ActivePlayerRow/ActivePlayerRow';
import styles from './ActivePlayersList.module.scss';

interface infoAboutOneActivePlayer{
    name: string,
    status: statusState
}
interface ActivePlayersListProps {
    className?: string,
    list?: infoAboutOneActivePlayer[],
}

export const ActivePlayersList = (props: ActivePlayersListProps) => {
  const { className, list } = props;

  return (
    <div className={classNames(styles.list, {}, [className])}>
      {list.map((el) => (<ActivePlayerRow key={el.name} name={el.name} status={el.status} />))}
    </div>
  );
};
