import { AuthByLoginForm } from 'features/AuthByLogin';
import React from 'react';
import { classNames } from 'shared/helpers';
import Dog from 'shared/public/contentImages/dog.png';
import { Window } from 'shared/UIKIT/Window/Window';
import styles from './AuthorizationWindow.module.scss';

export const AuthorizationWindow = () => (

  <Window className={classNames(styles.AuthorizationWindow)}>
    <img src={Dog} alt="dog" className={classNames(styles.image)} />
    <h2 className={classNames(styles.title)}>Войдите в игру</h2>
    <div className={styles.check}>
      <AuthByLoginForm className={styles.authForm} />
    </div>
  </Window>

);
