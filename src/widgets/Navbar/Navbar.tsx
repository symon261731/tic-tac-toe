import { useCallback, useState } from 'react';
import { observer } from 'mobx-react';
import { useLocation } from 'react-router-dom';
import { userMob } from 'entities/user';
import { Button, buttonTheme } from 'shared/UIKIT/Button/Button';
import { Logo } from 'shared/UIKIT/Logo/Logo';
import { NavLink } from 'shared/UIKIT/NavLink/NavLink';
import LogoutSVG from 'shared/public/svg/Exit.svg';
import BurgerSVG from 'shared/public/svg/BurgerIcon.svg';
import { classNames } from 'shared/helpers';
import styles from './Navbar.module.scss';

export const Navbar = observer(() => {
  const links = [
    { to: '/gameField', content: 'Игровое поле' },
    { to: '/rating', content: 'Рейтинг' },
    { to: '/activePlayers', content: 'Активные игроки' },
    { to: '/history', content: 'История игр' },
    { to: '/playerList', content: 'Список игроков' },
  ];

  const location = useLocation();

  const [burger, setBurger] = useState(false);

  const logout = () => {
    userMob.logout();
  };

  const clickOnBurger = useCallback(() => {
    setBurger(!burger);
  }, [burger]);

  return (
    <div className={styles.NavbarContainer}>
      <Logo />
      <nav className={classNames(styles.nav, { [styles.navAdaptive]: burger }, [])}>
        {links.map((oneLink) => (
          <NavLink
            key={oneLink.to}
            active={location.pathname === oneLink.to}
            to={oneLink.to}
            onClick={() => setBurger(false)}
          >
            {oneLink.content}
          </NavLink>
        ))}
      </nav>

      <Button
        onClick={logout}
        theme={buttonTheme.CLEAR}
        className={classNames(styles.logout, {}, [styles.hover])}
      >
        <LogoutSVG />
      </Button>

      <Button
        onClick={clickOnBurger}
        theme={buttonTheme.CLEAR}
        className={classNames(styles.burger, {}, [styles.hover])}
      >
        <BurgerSVG />
      </Button>
    </div>
  );
});
