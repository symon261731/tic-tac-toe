import React, { FC } from 'react';
import { classNames } from 'shared/helpers';
import styles from './RatingList.module.scss';

interface RatingListProps{
    className?: string,
    list?: any[],
}

export const RatingList: FC<RatingListProps> = (props) => {
  const { className, list } = props;
  // TODO Отрефакторить стили
  return (
    <table className={classNames(styles.table, {}, [className])}>
      <tr>
        <th className={classNames(styles.name, {}, [styles.weight])}>ФИО</th>
        <th className={classNames(styles.allGame, {}, [styles.weight])}>Всего игр</th>
        <th className={classNames(styles.win, {}, [styles.reset, styles.weight])}>Победы</th>
        <th className={classNames(styles.lose, {}, [styles.reset, styles.weight])}>Проигрыши</th>
        <th className={classNames(styles.persent, {}, [styles.weight])}>Процент побед</th>
      </tr>

      {list.map((onePlayerInfo) => (
        <tr key={onePlayerInfo.name} className={styles.row}>
          <th className={styles.name}>{onePlayerInfo.name}</th>
          <th className={styles.allGame}>{onePlayerInfo.allGames}</th>
          <th className={styles.win}>{onePlayerInfo.win}</th>
          <th className={styles.lose}>{onePlayerInfo.lose}</th>
          <th className={styles.persent}>{onePlayerInfo.persent}</th>
        </tr>
      ))}
    </table>
  );
};
