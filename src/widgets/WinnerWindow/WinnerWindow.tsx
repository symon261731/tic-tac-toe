import { classNames } from 'shared/helpers';
import { Modal } from 'shared/UIKIT/Modal/Modal';
import CupImage from 'shared/public/contentImages/cup.png';
import { Button, buttonColor } from 'shared/UIKIT/Button/Button';
import { useNavigate } from 'react-router-dom';
import { useCallback } from 'react';
import styles from './WinnerWindow.module.scss';

interface WinnerWindowProps {
    className?: string,
    isOpen: boolean,
    winnerName: string,
    onClose: ()=> void
}

export const WinnerWindow = (props: WinnerWindowProps) => {
  const {
    className, isOpen, onClose, winnerName,
  } = props;
  const navigate = useNavigate();
  const navigateMenu = useCallback(() => {
    navigate('/');
  }, []);
  const navigateNewGame = useCallback(() => {
    navigate('/activePlayers');
  }, []);

  return (
    <Modal isOpen={isOpen} onClose={onClose} className={styles.NoButtons}>
      <div className={classNames(styles.container, {}, [className])}>
        <img src={CupImage} alt="cup" className={styles.cupImage} />
        <h2 className={styles.title}>{`${winnerName} победил!`}</h2>
        <div className={styles.buttons}>
          <Button onClick={navigateNewGame}>Новая игра</Button>
          <Button onClick={navigateMenu} color={buttonColor.GREY}>Выйти в меню</Button>
        </div>
      </div>
    </Modal>
  );
};
