/* eslint-disable jsx-a11y/control-has-associated-label */
import { FC, useEffect, useState } from 'react';
import { classNames } from 'shared/helpers';
// import { HistoryListFakeData } from 'shared/mockData/mockData';
import { gameHistoryMob } from 'entities/GameHistory';
import Circle from 'shared/public/svg/smallCircle.svg';
import Cross from 'shared/public/svg/tic.svg';
import smallCup from 'shared/public/contentImages/smallCup.png';
import { Spinner } from 'shared/UIKIT/Spinner/Spinner';
import { observer } from 'mobx-react';
import styles from './HistoryList.module.scss';

interface HistoryListProps {
    className?: string
}

export const HistoryList: FC<HistoryListProps> = observer((props) => {
  const { className } = props;
  const { pending, error } = gameHistoryMob;
  const [historyList, setHistoryList] = useState([]);

  useEffect(() => {
    gameHistoryMob.fetchHistory().then((data) => setHistoryList(data));
  }, []);

  if (pending) return <Spinner />;
  if (error) return (<h2>{error}</h2>);
  return (
    <table className={classNames(styles.HistoryList, {}, [className])}>
      <tbody className={styles.tbody}>
        <tr>
          <th className={styles.listHeader}>Игроки</th>
          <th />
          <th />
          <th className={styles.listHeader}>Дата</th>
          <th className={styles.listHeader}>Время игры</th>
        </tr>
        {historyList?.map((oneGame) => (
          <tr className={styles.HistoryOneRow} key={oneGame.firstPlayer}>
            <th className={styles.player}>
              <Circle style={{ flex: '0 0 20px', height: '20px' }} />
              <p>
                {oneGame.firstPlayer}
              </p>
              {oneGame.winner === oneGame.firstPlayer ? <img src={smallCup} alt="winner" /> : null}
            </th>
            <th className={styles.versus}>против</th>
            <th className={styles.player}>
              <Cross style={{ flex: '0 0 16px', height: '16px' }} />
              <p>
                {oneGame.secondPlayer}
              </p>
              {oneGame.winner === oneGame.secondPlayer ? <img src={smallCup} alt="winner" /> : null}
            </th>
            <th className={styles.date}>{oneGame.date}</th>
            <th className={styles.date}>{oneGame.time}</th>
          </tr>
        ))}
      </tbody>
    </table>
  );
});
