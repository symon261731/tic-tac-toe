import React, { FC } from 'react';
import { classNames } from 'shared/helpers';
import Circle from 'shared/public/svg/smallCircle.svg';
import Cross from 'shared/public/svg/tic.svg';
import styles from './OppontensWindow.module.scss';

interface OppontensWindowProps {
    className?: string
}

export const OppontensWindow : FC<OppontensWindowProps> = (props: OppontensWindowProps) => {
  const { className } = props;
  const playerOne = {
    name: 'Пупкин Владлен Игоревич',
    statistic: '63',
  };
  const playerTwo = {
    name: 'Плюшкина Екатерина Викторовна',
    statistic: '43',
  };

  return (
    <div className={classNames(styles.opponentsWindow, {}, [className])}>
      <div className={classNames(styles.content)}>
        <h2 className={classNames(styles.title)}>Игроки</h2>
        <div className={classNames(styles.opponent)}>
          <Circle />
          <div className={classNames(styles.text)}>
            <p className={classNames(styles.name)}>{playerOne.name}</p>
            <p className={classNames(styles.statistic)}>{`${playerOne.statistic}% побед`}</p>
          </div>
        </div>
        <div className={classNames(styles.opponent)}>
          <Cross />
          <div className={classNames(styles.text)}>
            <p className={classNames(styles.name)}>{playerTwo.name}</p>
            <p className={classNames(styles.statistic)}>{`${playerTwo.statistic}% побед`}</p>
          </div>
        </div>
      </div>
    </div>
  );
};
