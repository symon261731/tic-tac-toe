import React from 'react';
import { RatingWindow } from 'widgets/RatingWindow/RatingWindow';
import styles from './RatingPage.module.scss';

const RatingPage = () => (
  <div className={styles.RatingPage}>
    <RatingWindow />
  </div>
);

export default RatingPage;
