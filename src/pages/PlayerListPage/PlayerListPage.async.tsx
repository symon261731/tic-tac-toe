import { lazy } from 'react';

export const PlayerListPageAsync = lazy(() => import('./PlayerListPage'));
