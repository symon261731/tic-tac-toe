import React, { useCallback, useState } from 'react';
import { AddPlayerModal } from 'features/AddPlayer';
import { classNames } from 'shared/helpers';
import { Button } from 'shared/UIKIT/Button/Button';
import { genderType, PlayerRow } from 'shared/UIKIT/PlayerRow/PlayerRow';
import { statusState } from 'shared/UIKIT/Status/Status';
import { Window } from 'shared/UIKIT/Window/Window';
import { playerListPageFakeData } from 'shared/mockData/mockData';
import styles from './PlayerListPage.module.scss';

interface playerInfo {
  name: string,
  age: number,
  gender: genderType,
  status: statusState.BLOKED | statusState.READY,
  created: string,
  changed: string,
}

const PlayerListPage = () => {
  const fakeData = playerListPageFakeData;
  const [isOpenModal, setIsOpenModal] = useState(false);

  const openModal = useCallback(() => {
    setIsOpenModal(true);
  }, [isOpenModal]);

  const closeModal = useCallback(() => {
    setIsOpenModal(false);
  }, [isOpenModal]);

  return (
    <div className={classNames(styles.PlayerListPage)}>
      <AddPlayerModal isOpen={isOpenModal} onClose={closeModal} />
      <Window width={1416} center className={styles.windowPosition}>
        <div className={classNames(styles.headerContent)}>
          <h2 className={styles.title}>Список игроков</h2>
          <Button onClick={openModal}>Добавить игрока</Button>
        </div>
        <div>
          { fakeData.map((onePlayer: playerInfo) => (
            <PlayerRow
              key={onePlayer.name}
              name={onePlayer.name}
              age={onePlayer.age}
              gender={onePlayer.gender}
              status={onePlayer.status}
              created="12 октября 2021"
              changed="12 октября 2021"
            />
          ))}

        </div>
      </Window>
    </div>
  );
};

export default PlayerListPage;
