import { Window } from 'shared/UIKIT/Window/Window';
import { HistoryList } from 'widgets/HistoryList/HistoryList';
import styles from './HistoryPage.module.scss';

const HistoryPage = () => (
  <div className={styles.historyPage}>
    <Window width={1074} center className={styles.window}>
      <h2 className={styles.title}>История игр</h2>
      <HistoryList />
    </Window>
  </div>
);

export default HistoryPage;
