import React, {
  useCallback, useEffect, useState,
} from 'react';
import { classNames } from 'shared/helpers';
import { statusState } from 'shared/UIKIT/Status/Status';
import { SwitchToggle } from 'shared/UIKIT/SwitchToggle/SwitchToggle';
import { Window } from 'shared/UIKIT/Window/Window';
import { ActivePlayersList } from 'widgets/ActivePlayersList/ActivePlayersList';
import { activePlayerPageFakeData } from 'shared/mockData/mockData';
import styles from './ActivePlayersPage.module.scss';

const ActivePlayersPage = () => {
  // TODO ГОВНОКОДИМ ОТРЕФАКТОРИТЬ

  const [fakeData, setFakeData] = useState([]);
  const [check, setCheck] = useState(false);

  const filterPlayers = useCallback(() => {
    setCheck(!check);
  }, [check]);

  useEffect(() => {
    // типо запрос
    const data = activePlayerPageFakeData;
    if (check) {
      setFakeData((prev) => prev.filter((el) => el.status === statusState.READY));
    } else {
      setFakeData(data);
    }
  }, [check]);

  return (
    <Window width={781} center className={styles.ActivePlayerPage}>
      <div className={styles.activePlayerHeader}>
        <h2 className={classNames(styles.title)}>Активные игроки</h2>
        <div className={classNames(styles.textToggle)}>
          <p className={classNames(styles.text)}>Только свободные</p>
          <SwitchToggle checked={check} changeChecked={filterPlayers} />
        </div>
      </div>
      <ActivePlayersList list={fakeData} />
    </Window>
  );
};

export default ActivePlayersPage;
