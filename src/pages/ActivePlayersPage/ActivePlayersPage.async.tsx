import { lazy } from 'react';

export const ActivePlayersPageAsync = lazy(() => import('./ActivePlayersPage'));
