import { lazy } from 'react';

export const GameFieldPageAsync = lazy(() => import('./GameFieldPage'));
