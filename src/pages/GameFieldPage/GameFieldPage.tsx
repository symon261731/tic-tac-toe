import { gameFieldMob } from 'entities/GameField';
import { observer } from 'mobx-react';
import { TurnSize } from 'shared/UIKIT/TurnSize/TurnSize';
import { Chat } from 'widgets/Chat/Chat';
import { GameBoard } from 'widgets/GameBoard/GameBoard';
import { OppontensWindow } from 'widgets/OpponentsWindow/OpponentsWindow';
import { Timer } from 'widgets/Timer/Timer';
import { WinnerWindow } from 'widgets/WinnerWindow/WinnerWindow';
import styles from './GameFieldPage.module.scss';

const GameFieldPage = observer(() => {
  const { winnerName, isGameOver: winnerModal } = gameFieldMob;
  return (
    <>
      <WinnerWindow
        isOpen={winnerModal}
        winnerName={winnerName}
        onClose={() => 'никакого закрытия модалки'}
      />
      <div className={styles.GameFieldPage}>
        <div className={styles.GameField}>
          <OppontensWindow className={styles.alignOpponentsWindow} />
          <div className={styles.centralContent}>
            <Timer />
            <GameBoard />
            <TurnSize />
          </div>
          <Chat className={styles.chat} />
        </div>
      </div>
    </>
  );
});

export default GameFieldPage;
