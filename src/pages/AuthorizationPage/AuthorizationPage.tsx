import React from 'react';
import { AuthorizationWindow } from 'widgets/AuthorizationWindow/AuthorizationWindow';
import styles from './AuthorizationPage.module.scss';

const AuthorizationPage = () => (
  <div className={styles.authPage}>
    <AuthorizationWindow />
  </div>
);

export default AuthorizationPage;
