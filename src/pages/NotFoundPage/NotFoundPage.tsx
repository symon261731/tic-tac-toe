import React from 'react';
import { classNames } from 'shared/helpers';
import styles from './NotFoundPage.module.scss';

const NotFoundPage = () => (
  <div className={classNames(styles.NotFoundPage)}>
    <h1 className={classNames(styles.title)}>
      Извините но такой страницы не существует
    </h1>
  </div>
);

export default NotFoundPage;
