import { ActivePlayersPageAsync } from './ActivePlayersPage/ActivePlayersPage.async';
import { AuthorizationPageAsync } from './AuthorizationPage/AuthorizationPage.async';
import { GameFieldPageAsync } from './GameFieldPage/GameFieldPage.async';
import { HistoryPageAsync } from './HistoryPage/HistoryPage.async';
import { RatingPageAsync } from './RatingPage/RatingPage.async';
import { PlayerListPageAsync } from './PlayerListPage/PlayerListPage.async';
import { NotFoundPageAsync } from './NotFoundPage/NotFoundPage.async';

export {
  ActivePlayersPageAsync,
  AuthorizationPageAsync,
  GameFieldPageAsync,
  HistoryPageAsync,
  RatingPageAsync,
  PlayerListPageAsync,
  NotFoundPageAsync,
};
