import { Configuration } from 'webpack';
import buildDevServer from './buildDevServer/buildDevServer';
import { buildLoaders } from './buildLoader/buildLoader';
import { buildPlugins } from './buildPlugins/buildPlugins';
import { optionTypes } from './types/webpackTypes';

export function webpackConfig(options: optionTypes): Configuration {
  const {
    paths, mode, isDev, port,
  } = options;

  return {
    mode,

    entry: paths.pathToEntry,

    devtool: isDev ? 'inline-source-map' : undefined,

    module: {
      rules: buildLoaders(isDev),
    },

    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      preferAbsolute: true,
      modules: [paths.pathToSrc, 'node_modules'],
    },

    plugins: buildPlugins(paths.pathToHtml),

    output: {
      filename: '[name].[contenthash].js',
      path: paths.pathOfBuild,
      clean: true,
    },

    devServer: isDev ? buildDevServer(port) : undefined,
  };
}
