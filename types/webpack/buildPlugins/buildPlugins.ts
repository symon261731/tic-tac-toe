import HtmlWebpackPlugin from 'html-webpack-plugin';
import { ProgressPlugin } from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export function buildPlugins(pathToHtml: string) {
  const plugins = [];

  plugins.push(new HtmlWebpackPlugin({ template: pathToHtml }));
  plugins.push(new MiniCssExtractPlugin({
    filename: 'css/[name].[contenthash: 8].css',
    chunkFilename: 'css/[name].[contenthash: 8].css',
  }));
  plugins.push(new ProgressPlugin());

  return plugins;
}
