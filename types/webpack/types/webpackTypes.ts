export interface pathsProps {
    pathToHtml: string,
    pathToSrc: string,
    pathToEntry: string,
    pathOfBuild: string,
}

export interface optionTypes {
    mode: 'development' | 'production'
    paths : pathsProps,
    isDev : boolean,
    port: number,
}
